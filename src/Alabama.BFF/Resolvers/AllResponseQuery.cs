﻿using System.Collections.Generic;
using System.Linq;
using Alabama.BFF.Domain;
using Alabama.BFF.Domain.Interfaces;
using Alabama.BFF.Schemas;
using GraphQL.Types;

namespace Alabama.BFF.Resolvers
{
    public class AllResponseQuery : ObjectGraphType
    {
        public TreasuryDirectBalance TreasuryDirect { get; set; }
        public IList<FixedIncomeBalance> FixedIncome { get; set; }
        public AllResponseQuery(ICustodyTreasuryDirectRepository custodyTreasury, ICustodyFixedIncomeRepository custodyFixedIncome, string token)
        {
            TreasuryDirect = custodyTreasury.GetBalance(token).Result;
            FixedIncome = custodyFixedIncome.GetBalance(token).Result;

            Field<TreasuryDirectBalanceType>("treasurydirectBalance", resolve: context => { return TreasuryDirect; });
            Field<ListGraphType<FixedIncomeType>>("fixedIncomeBalance", resolve: context => { return FixedIncome; });
            Field<FloatGraphType>("sumedFixedIncome", resolve: context => { return FixedIncome.Sum(o => o.Balance);  });
        }
    }
}
