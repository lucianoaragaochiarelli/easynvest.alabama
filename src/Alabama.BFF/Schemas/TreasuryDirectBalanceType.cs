﻿using Alabama.BFF.Domain;
using GraphQL.Types;

namespace Alabama.BFF.Schemas
{
    public class TreasuryDirectBalanceType : ObjectGraphType<TreasuryDirectBalance>
    {
        public TreasuryDirectBalanceType()
        {
            Field(_ => _.Account).Name("accountNumber");
            Field(_ => _.InvestedCapital).Name("amount");
        }
    }
}
