﻿using System;
using GraphQL.Types;

namespace Alabama.BFF.Schemas
{
    public class AllResponseSchema : Schema, ISchema
    {
        public AllResponseSchema(IServiceProvider service) : base(service)
        {
            //Não utilizado por conta de utilizar passando o token instanciando diretamente no Controller
            //Query = service.GetRequiredService<AllResponseQuery>();
        }
    }
}
