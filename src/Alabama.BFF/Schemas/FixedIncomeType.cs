﻿using Alabama.BFF.Domain;
using GraphQL.Types;

namespace Alabama.BFF.Schemas
{
    public class FixedIncomeType : ObjectGraphType<FixedIncomeBalance>
    {
        public FixedIncomeType()
        {
            Field(_ => _.Balance).Name("amount");
            Field(_ => _.SecurityType).Name("securityType");
        }
    }
}
