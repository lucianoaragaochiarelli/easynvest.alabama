﻿using GraphQL.Types;

namespace Alabama.BFF.Schemas
{
    public class FixedIncomeBalanceType : ObjectGraphType
    {
        public FixedIncomeBalanceType()
        {
            Field<ListGraphType<FixedIncomeType>>("fixedIncomeBalance");
        }
    }
}
