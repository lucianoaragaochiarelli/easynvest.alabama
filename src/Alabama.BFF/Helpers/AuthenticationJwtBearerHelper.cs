﻿using Microsoft.AspNetCore.Http;

namespace Alabama.BFF.Helpers
{
    public static class AuthenticationJwtBearerHelper
    {
        public static string GetToken(this HttpRequest request)
        {
            var index = "bearer ".Length;
            var token = request.Headers["Authorization"].ToString();
            return token.Substring(index, token.Length - index);
        }
    }
}
