using Alabama.BFF.Domain.Interfaces;
using Alabama.BFF.Repositories;
using Alabama.BFF.Schemas;
using GraphQL;
using GraphQL.NewtonsoftJson;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Alabama.BFF
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient<ICustodyTreasuryDirectRepository, CustodyTreasuryDirectRepository>();
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            services.AddSingleton<ISchema, AllResponseSchema>();
            services.AddSingleton<TreasuryDirectBalanceType>();
            services.AddSingleton<FixedIncomeType>();
            services.AddTransient<ICustodyTreasuryDirectRepository, CustodyTreasuryDirectRepository>();
            services.AddTransient<ICustodyFixedIncomeRepository, CustodyFixedIncomeRepository>();
            //services.AddSingleton<AllResponseQuery>();
            services.AddControllers();
        }

            
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
