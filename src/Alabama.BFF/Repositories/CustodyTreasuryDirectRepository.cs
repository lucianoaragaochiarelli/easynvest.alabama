﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using Alabama.BFF.Domain;
using Alabama.BFF.Domain.Interfaces;

namespace Alabama.BFF.Repositories
{
    public class CustodyTreasuryDirectRepository : ICustodyTreasuryDirectRepository
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly string URI = "http://custody-treasurydirect.kube.dev.easynvest.io/v1/balance";

        public CustodyTreasuryDirectRepository(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<TreasuryDirectBalance> GetBalance(string token)
        {
            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.GetAsync(URI);

            return JsonSerializer.Deserialize<TreasuryDirectBalance>(await response.Content.ReadAsStringAsync());
        }
    }
}
