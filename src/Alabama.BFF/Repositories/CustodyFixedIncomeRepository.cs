﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using Alabama.BFF.Domain;
using Alabama.BFF.Domain.Interfaces;

namespace Alabama.BFF.Repositories
{
    public class CustodyFixedIncomeRepository : ICustodyFixedIncomeRepository
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly string URI = "http://custody-fixedincome.kube.dev.easynvest.io/v2/custody/balance";

        public CustodyFixedIncomeRepository(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IList<FixedIncomeBalance>> GetBalance(string token)
        {
            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            
            var response = await client.GetAsync(URI);

            var result = JsonSerializer.Deserialize<IList<FixedIncomeBalance>>(await response.Content.ReadAsStringAsync(), new JsonSerializerOptions
            {
                AllowTrailingCommas = true,
                PropertyNameCaseInsensitive = true
            });

            return result;
        }
    }
}
        

