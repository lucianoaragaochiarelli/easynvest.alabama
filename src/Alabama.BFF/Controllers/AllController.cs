﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Alabama.BFF.Domain.Interfaces;
using Alabama.BFF.Helpers;
using Alabama.BFF.Resolvers;
using Alabama.BFF.Schemas;
using GraphQL;
using GraphQL.NewtonsoftJson;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Alabama.BFF.Controllers
{
    [Route("[controller]")]
    public class AllController : Controller
    {
        private readonly ISchema _schema;
        private readonly IDocumentExecuter _executer;
        private readonly IDocumentWriter _documentWriter;
        private readonly ICustodyTreasuryDirectRepository _treasuryDirectRepository;
        private readonly ICustodyFixedIncomeRepository _fixedIncomeRepository;

        public AllController(ISchema schema, IDocumentExecuter executer, IDocumentWriter documentWriter, ICustodyTreasuryDirectRepository treasuryDirectRepository, ICustodyFixedIncomeRepository fixedIncomeRepository)
        {
            _schema = schema;
            _executer = executer;
            _documentWriter = documentWriter;
            _treasuryDirectRepository = treasuryDirectRepository;
            _fixedIncomeRepository = fixedIncomeRepository;
        }

        [HttpPost]
        public async Task GetAll([FromBody] SchemaConfig query)
        {
            var schema = new Schema()
            {
                Query = new AllResponseQuery(_treasuryDirectRepository, _fixedIncomeRepository, Request.GetToken())
            };


            var result = await _executer.ExecuteAsync(_ =>
            {
                _.Schema = schema;
                _.Query = query.Query;
                _.Inputs = query.Variables?.ToInputs();
            });

            await WriteResponseAsync(HttpContext, result);
        }

        private async Task WriteResponseAsync(HttpContext context, ExecutionResult result)
        {
            context.Response.ContentType = "application/json";
            if (result.Errors?.Any() == true)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Console.WriteLine(result.Errors[0]);
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.OK;
            }

            await _documentWriter.WriteAsync(context.Response.Body, result);
        }
    }
}
