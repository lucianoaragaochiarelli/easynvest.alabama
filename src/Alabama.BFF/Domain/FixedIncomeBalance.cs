﻿namespace Alabama.BFF.Domain
{
    public class FixedIncomeBalance
    {
        public double Balance { get; set; }

        public string SecurityType { get; set; }
    }
}
