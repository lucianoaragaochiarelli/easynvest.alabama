﻿using System.Threading.Tasks;

namespace Alabama.BFF.Domain.Interfaces
{
    public interface ICustodyTreasuryDirectRepository
    {
        Task<TreasuryDirectBalance> GetBalance(string token);
    }
}
