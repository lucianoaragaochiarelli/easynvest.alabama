﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alabama.BFF.Domain.Interfaces
{
    public interface ICustodyFixedIncomeRepository
    {
        Task<IList<FixedIncomeBalance>> GetBalance(string token);
    }
}
