﻿namespace Alabama.BFF.Domain
{
    public class TreasuryDirectBalance
    {
        public int Account { get; set; }
        public double InvestedCapital { get; set; }
    }
}
